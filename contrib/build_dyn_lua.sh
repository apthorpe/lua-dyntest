#!/bin/sh

echo "-- Unpacking --"
tar xfz lua-5.3.6.tar.gz

echo "-- In Lua dist root --"
cd lua-5.3.6

echo "-- Building Lua --"
make linux

echo "-- make test --"
make test

echo "-- Installing locally --"
make local

echo "-- lua -v --"
./install/bin/lua -v

echo "-- In dyntest root --"
cd ..

echo "-- mylib --"
gcc -Wall mylib.c -I ./lua-5.3.6/src -fPIC -shared -o mylib.so 
./lua-5.3.6/install/bin/lua -lmylib

echo "-- dummy --"
gcc -Wall dummy.c -I ./lua-5.3.6/src -fPIC -shared -o dummy.so 
./lua-5.3.6/install/bin/lua -ldummy

echo "-- dummy1 --"
gcc -Wall dummy1.c -I ./lua-5.3.6/src -fPIC -shared -o dummy1.so 
./lua-5.3.6/install/bin/lua -ldummy1

echo "-- dummy2 --"
gcc -Wall dummy2.c -I ./lua-5.3.6/src -fPIC -shared -o dummy2.so 
./lua-5.3.6/install/bin/lua -ldummy2

echo "-- test01 --"
gcc -Wall test01.c -I ./lua-5.3.6/src -llua -lstdc++ -o test01 -lm -ldl -I ./lua-5.3.6/src 
./test01

