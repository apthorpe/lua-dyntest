#include <stdio.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

static int l_sin(lua_State *L) 
{   
    double d = luaL_checknumber(L, 1); 
    lua_pushnumber(L, sin(d));  /* push result */

    return 1;  /* number of results */
}


static const struct luaL_Reg mylib[] = { 
    {"mysin", l_sin},
    {NULL, NULL}
};

extern int luaopen_mylib(lua_State* L)
{
    luaL_newlib(L, mylib);

    return 1;
}
