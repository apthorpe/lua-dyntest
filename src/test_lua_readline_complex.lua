local os = require("os")
local readline = require("readline")

if not (readline.read_history(nil)) then -- file probably doesn't exist
  assert(readline.write_history(nil)) -- create the default history file
end

local prompt = 'FAKESH> '

local builtins = {
  ['exit'] = function()
    os.exit(0)
  end,
}

while (true) do
  local cmdline = readline.readline(prompt)

  readline.add_history(cmdline)

  -- append most recent line to default history file
  assert(readline.append_history(1, nil))

  if (builtins[cmdline] ~= nil) then
    builtins[cmdline]()
    goto continue
  end

  os.execute(cmdline)

  ::continue::
end


