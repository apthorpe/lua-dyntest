#include<windows.h>
#include<math.h>
#include "lauxlib.h"
#include "lua.h"

/* From https://stackoverflow.com/questions/43980470/how-to-create-dll-of-lua-module */

static int IdentityMatrix(lua_State *L)
{
    int in = lua_gettop(L);
    if (in!=1)
    {
       lua_pushstring(L,"Maximum 1 argument");
       lua_error(L);
    }
    lua_Number n = lua_tonumber(L,1);
    lua_newtable(L);                  /*                 tabOUT n */
    int i,j;
    for (i=1;i<=n;i++)
    {
        lua_newtable(L);              /*         row(i) tabOUT n */
        lua_pushnumber(L,i);          /*       i row(i) tabOUT n */
        for (j=1;j<=n;j++)
        {
            lua_pushnumber(L,j);      /*     j i row(i) tabOUT n */
            if (j==i)
            {
                lua_pushnumber(L,1);
            }
            else                      /* 0/1 j i row(i) tabOUT n */
            {
                lua_pushnumber(L,0);
            }
            /*  Put 0/1 inside row(i) at j position */
            lua_settable(L,-4);       /*       i row(i) tabOUT n */
        }
        lua_insert(L,-2);             /*       row(i) i tabOUT n */

        /* Insert row(i) into position in tabOUT */
        lua_settable(L,2);            /*                tabOUT n */
    }
    return 1;
}


static const struct luaL_Reg LuaMath [] = {{"IdentityMatrix", IdentityMatrix},
                                           {            NULL,           NULL}};

int __declspec(dllexport) luaopen_LuaMath(lua_State *L)
{
    luaL_newlib(L,LuaMath);
    return 1;
}
