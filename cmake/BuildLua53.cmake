# Retrieve Lua 5.3 from external source and include into project;
# see https://www.lua.org/
#
# The overall process is to build Lua 5.3 using its Makefile with the
# necessary options (e.g. enable shared libraries). An in-source build
# is performed so Windows support files can be built from freshly
# generated intermediate files.
#
# Windows support files are built by
# add_custom_command(TARGET LUA53_external POST_BUILD)
# This process requires the MinGW tools `ar`, `dlltool`, and `gendef`:
# - `ar` and `dlltool` are contained in MSYS2 packages msys2-devel or binutils
#   and are probably on path with `gcc`
# - `gendef` is contained in MSYS2 package mingw-w64-x86_64-tools or (32-bit) mingw-w64-i686-tools
#   and are probably not on path. Specify CMake option MINGW_ROOT_DIR
#   which will put the full path of `gendef` in GENDEF_EXE
#
# The following output variables are set by this recipe and used
# downstream:
#   General:
#     LUA53_SOURCE_DIR        - root of Lua 5.3 source dist
#     LIBLUA53_SOURCE_DIR     - 'src' directory under dist root
#     LUA53_LOCAL_INSTALL_DIR - local installation root
#     LUA53_INCLUDE_DIR       - contains Lua 5.3 header files (local install)
#     LUA53_LIB_DIR           - contains Lua 5.3 library files (local install)
#     LUA53_STATIC_LIBRARY    - path to Lua 5.3 static library (local install)
#   Windows:
#     LUA53_LIBRARY_DLL       - Path to locally installed lua53.dll
#     LUA53_STATIC_IMPORT_LIB - Path to locally installed lua53.dll.a
#     LUA53_IMPORT_LIBFILE    - Path to locally installed lua53.lib
#     LUA53_EXPORT            - Path to locally installed lua53.exp
#     LUA53_DEFFILE           - Path to locally installed lua53.def
#   Note: append _FN to get bare filename of Windows support files,
#         e.g. $LUA53_LIBRARY_DLL_FN = 'lua53.dll'

set(LUA53_SOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}/lua53-source")
set(LIBLUA53_SOURCE_DIR "${LUA53_SOURCE_DIR}/src")

set(LUA53_LOCAL_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/lua53-artifacts")

# find_program(MAKE_EXECUTABLE NAMES make gmake)
set(MAKE_EXECUTABLE make)
unset(LUA53_MAKE_ARGS)
if(WIN32)
    set(PLAT mingw)
    list(APPEND LUA53_MAKE_ARGS
        "MYCFLAGS='-DLUA_DL_DLL'"
        )
elseif(APPLE)
    set(PLAT macosx)
elseif(LINUX)
    set(PLAT linux)
else()
    set(PLAT posix)
endif()

# Note: URL_HASH was generated locally from lua-5.3.6.tar.gz
# and should match the archive file. Update URL_HASH if URL is changed
ExternalProject_Add(
    LUA53_external
    # Note: Use URL and URL_HASH [SHA512|SHA256|MD5]=4A54C0DE... to
    # download and checksum an archive. Note that URL may refer to a
    # local file, allowing this to work without net access.
    URL                    https://www.lua.org/ftp/lua-5.3.6.tar.gz
    URL_HASH               MD5=83f23dbd5230140a3770d5f54076948d
    SOURCE_DIR             ${LUA53_SOURCE_DIR}

    CONFIGURE_COMMAND      ""
    BUILD_IN_SOURCE        TRUE
    BUILD_COMMAND          ${MAKE_EXECUTABLE} ${PLAT} ${LUA53_MAKE_ARGS}
    # BUILD_COMMAND          ${CMAKE_COMMAND} -E env ${LUA53_MYCFLAGS} ${MAKE_EXECUTABLE} ${PLAT}
    # BUILD_COMMAND          ${LUA53_MYCFLAGS} ${MAKE_EXECUTABLE} ${PLAT}
    # INSTALL_COMMAND        "${MAKE_EXECUTABLE} install INSTALL_TOP=${LUA53_LOCAL_INSTALL_DIR}"
    INSTALL_COMMAND        ${MAKE_EXECUTABLE} install INSTALL_TOP=<INSTALL_DIR>

    INSTALL_DIR            ${LUA53_LOCAL_INSTALL_DIR}
    # CMAKE_ARGS             ${LUA53_CMAKE_ARGS}
    # BUILD_BYPRODUCTS       ${LUA53_LIBFILE}
    LOG_BUILD              YES
    TIMEOUT                30
    USES_TERMINAL_DOWNLOAD YES
    USES_TERMINAL_UPDATE   YES
)

set (LUA53_INCLUDE_DIR    "${LUA53_LOCAL_INSTALL_DIR}/include")
set (LUA53_LIB_DIR        "${LUA53_LOCAL_INSTALL_DIR}/lib")
# This file is created by original Lua build and local installation process
set (LUA53_STATIC_LIBRARY "${LUA53_LIB_DIR}/liblua.a")
if(WIN32)
    # Set names of all needed and produced files
    # Note: These are bare filenames; paths will be added later as needed)
    # Libraries (of a sort)
    set(LUA53_LIBRARY_DLL_FN       "lua53.dll")
    set(LUA53_STATIC_IMPORT_LIB_FN "liblua53.dll.a")
    set(LUA53_IMPORT_LIBFILE_FN    "lua53.lib")
    set(LUA53_EXPORT_FN            "lua53.exp")
    # Includes (of a sort)
    set(LUA53_DEFFILE_FN           "lua53.def")

    # Set local installation (lua53-artifact) paths; use these downstream
    # Libraries (of a sort)
    set(LUA53_LIBRARY_DLL       "${LUA53_LIB_DIR}/${LUA53_LIBRARY_DLL_FN}")
    set(LUA53_STATIC_IMPORT_LIB "${LUA53_LIB_DIR}/${LUA53_STATIC_IMPORT_LIB_FN}")
    set(LUA53_IMPORT_LIBFILE    "${LUA53_LIB_DIR}/${LUA53_IMPORT_LIBFILE_FN}")
    set(LUA53_EXPORT            "${LUA53_LIB_DIR}/${LUA53_EXPORT_FN}")
    # Includes (of a sort)
    set(LUA53_DEFFILE           "${LUA53_INCLUDE_DIR}/${LUA53_DEFFILE_FN}")

    # Requires ar and dlltool - contained in MSYS2 packages msys2-devel or binutils
    #                         - is probably on path with gcc
    # Requires gendef - contained in MSYS2 package mingw-w64-x86_64-tools or (32-bit) mingw-w64-i686-tools
    #                 - probably not on path so specify full path in ${GENDEF_EXE}
    add_custom_command(TARGET LUA53_external
        POST_BUILD
        WORKING_DIRECTORY "${LIBLUA53_SOURCE_DIR}"
        # ar creates LUA53_IMPORT_LIBFILE_FN
        # Brittle, specific to Lua 5.3 dist, but what option is there?
        COMMAND ar rcs -o ${LUA53_IMPORT_LIBFILE_FN}
            lapi.o lauxlib.o lbaselib.o lbitlib.o lcode.o lcorolib.o lctype.o
            ldblib.o ldebug.o ldo.o ldump.o lfunc.o lgc.o linit.o liolib.o
            llex.o lmathlib.o lmem.o loadlib.o lobject.o lopcodes.o loslib.o
            lparser.o lstate.o lstring.o lstrlib.o ltable.o ltablib.o ltm.o
            lundump.o lutf8lib.o lvm.o lzio.o

        # gendef creates LUA53_DEFFILE
        COMMAND ${GENDEF_EXE} ${LUA53_LIBRARY_DLL_FN}

        # dlltool creates LUA53_EXPORT and LUA53_STATIC_IMPORT_LIB
        # Note: -D expects a file name without path
        COMMAND dlltool
            -d ${LUA53_DEFFILE_FN}
            -D ${LUA53_LIBRARY_DLL_FN}
            -e ${LUA53_EXPORT_FN}
            -l ${LUA53_STATIC_IMPORT_LIB_FN}

        # Put windows support files in local install tree
        COMMAND "${CMAKE_COMMAND}" -E copy ${LUA53_IMPORT_LIBFILE_FN}    ${LUA53_IMPORT_LIBFILE}
        COMMAND "${CMAKE_COMMAND}" -E copy ${LUA53_LIBRARY_DLL_FN}       ${LUA53_LIBRARY_DLL}
        COMMAND "${CMAKE_COMMAND}" -E copy ${LUA53_DEFFILE_FN}           ${LUA53_DEFFILE}
        COMMAND "${CMAKE_COMMAND}" -E copy ${LUA53_EXPORT_FN}            ${LUA53_EXPORT}
        COMMAND "${CMAKE_COMMAND}" -E copy ${LUA53_STATIC_IMPORT_LIB_FN} ${LUA53_STATIC_IMPORT_LIB}

        BYPRODUCTS
            ${LUA53_STATIC_IMPORT_LIB_FN}
            ${LUA53_IMPORT_LIBFILE_FN}
            ${LUA53_DEFFILE_FN}
            ${LUA53_EXPORT_FN}
        )

        # Note: The local install tree is added to the global install tree,
        # handled by install() in the top-level CMakeLists.txt
endif()

# To use this recipe, add one of the following include() lines
# to CMakeLists.txt after project():
#     include(BuildLua53)
# or
#     include(/path/to/BuildLua53.cmake)

# __END__