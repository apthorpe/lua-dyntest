# lua-dyntest

A CMake build script for building Lua with dynamic module support along
with confirmatory test code.

## Motivation

Theoretically, it's possible to build C modules which can be dynamically
loaded into Lua with `require "my_module_name"`. This is much more
difficult than the scatteted documentation suggests. Further, debugging
C modules can be maddening because it's very difficult to determine
whether the problem lies in the compiled C modules or in the local Lua build.

This project is intended to alleviate this frustration and act as a
functional proof-of-concept for multiple common platforms.

## Limitations

This is an experiment and work in progress based on a (Linux) shell
script that is confirmed to produce a working Lua installation along
with working loadable libraries; see `contrib/build_dyn_lua.sh`.

## Author

Bob Apthorpe <bob.apthorpe@gmail.com>

## License

MIT